---
title: "Potoo"
subtitle: "Example Presentation"
date: "Right now"
author: "Your's truly"
---

<!-- Prettier cannot handle pandoc div notation we therefore disable it as soon as possible -->
<!-- prettier-ignore-start -->
## Introduction

*Potoo* allows you to easily present your markdown based (non-graphical) content.

## This is a slide title

- This is
- a list

**Tables**

| Flavour    | UK Release Date  | Director     |
| ---------- | ---------------- | ------------ |
| Strawberry | 2004-04-09       | Edgar Wright |

## New slide (title)

Fragments are incrementally displayed parts of a slide

- Like this

. . .

- for example

. . .

You can trigger them anytime by inserting a single line consisting of

```markdown
. . .
```

---

The following line triggers a new slide

```markdown
---
```

### h3 slide title (`###`)

Given the default value of `-s/--header-split-level` this should also trigger a new slide.

#### h4

##### h5

###### h6


<!-- prettier-ignore-end -->
